package main

import "fmt"

type Machine interface {
	Action() string
}

type Ingredients interface {
	Recipe() string
}

type Drink struct {
	a string
	b string
	c string
}

func (product Drink) Action() string {
	return "Uruchom maszyne i upewnij się że w pojemniku jest " + product.a + ", następnie upewnij się że w mieszadle jest " + product.b + " ,wykonaj recepturę i na koniec sprawdź czy jest " + product.c + "."
}

func (product Drink) Recipe() string {
	return "Jeśli jest " + product.a + ", podgrzej i z pojemnika gdzie jest " + product.b + " ,wlej do kubka jednocześnie dodając " + product.c + ", wymieszaj ponownie i podaj napój."
}

func main() {
	c := Drink{"woda", "kawa", "cukier"}
	var s Machine = c
	var o Ingredients = c
	fmt.Println("Akcja maszyny: " + s.Action())
	fmt.Println("Receptura: " + o.Recipe())
}
