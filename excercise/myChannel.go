package main

import "fmt"

func main() {
	num := make(chan int, 50) // stwórz kanał o pojemności 50 int

	// wstaw wartość do kanału
	go func() {
		//wyślij wartość od 1-50 do kanału
		for i := 1; i <= 50; i++ {
			num <- i // zapętlij wewnątrz goroutine
		}
		//close, oznacza to, że operacja wysyłania została zakończona i zamyka kanał
		close(num)
	}()

	//pobierz wartość z kanału za pomocą pętli for
	for x := range num {
		fmt.Println("Liczba z num:", x)
	}

	fmt.Println("Gotowe..!")
}
